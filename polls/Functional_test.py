from __future__ import absolute_import

import os
from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.by import By


class FunctionalTest(TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome('D:\Users\Asus\Documents\chromedriver.exe')
        self.browser.set_window_size(1024, 768)
        self.browser.implicitly_wait(5000)

    def tearDown(self):
        self.browser.quit()

    def test_1_title(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn('Busco Ayuda', self.browser.title)

    def test_registro_independiente(self):
        self.browser.get('http://127.0.0.1:8000/')
        link = self.browser.find_element_by_id('id_register')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.send_keys('Adriana')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Bonilla')

        experiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        experiencia.send_keys('3')

        self.browser.find_element_by_xpath(
            "//select[@id='id_tiposDeServicio']/option[text()='TiposDeServicio object']").click()
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3016951215')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('ap.bonilla@uniandes.edu.co')

        nombreUsuario = self.browser.find_element_by_id('id_username')
        nombreUsuario.send_keys('ap.bonil9')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('12345678')

        imagen = self.browser.find_element_by_id('id_imagen')
        imagen.send_keys(
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ05VMx8IQ076_-RLMUnbks_lyGTa-5ZKN3xlsSstInNE5R1RtAFg")

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)
        url = self.browser.current_url;

        self.assertIn('http://127.0.0.1:8000/', url)

    def test_verDetalle(self):
        self.browser.get('http://localhost:8000')
        span=self.browser.find_element(By.XPATH, '//span[text()="Adriana Bonilla"]')
        span.click()
        h2=self.browser.find_element(By.XPATH, '//h2[text()="Adriana Bonilla"]')
        self.assertIn('Adriana Bonilla', h2.text)

    def test_login(self):
        self.browser.get('http://localhost:8000/')
        link = self.browser.find_element_by_id('id_login')
        link.click()
        user = self.browser.find_element_by_id('user')
        user.send_keys('ap.bonil9')
        pwd = self.browser.find_element_by_id('pwd')
        pwd.send_keys('12345678')
        botonGrabar = self.browser.find_element_by_id('id_ingresar')
        botonGrabar.click()
        #self.browser.implicitly_wait(3)
        #url = self.browser.current_url;
        #userauth = self.browser.find_element_by_id('id_userlabel')
        #print(userauth)
        #self.assertIn('http://localhost:8000/', url)

    def test_zeditar(self):
        self.browser.get('http://localhost:8000/')
        link = self.browser.find_element_by_id('id_login')
        link.click()
        user = self.browser.find_element_by_id('user')
        user.send_keys('ap.bonil9')
        pwd = self.browser.find_element_by_id('pwd')
        pwd.send_keys('12345678')
        botonGrabar = self.browser.find_element_by_id('id_ingresar')
        botonGrabar.click()
        self.browser.implicitly_wait(8)
        url = self.browser.current_url;
        #userauth = self.browser.find_element_by_id('id_userlabel')
        #print(userauth)
        #self.assertIn('http://localhost:8000/', url)
        #self.browser.get('http://127.0.0.1:8000/')
        link = self.browser.find_element_by_id('id_editar')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.send_keys('Huber')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Galviz')

        experiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        experiencia.send_keys('3')

        self.browser.find_element_by_xpath(
            "//select[@id='id_tiposDeServicio']/option[text()='TiposDeServicio object']").click()
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3016951215')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('ap.bonilla@uniandes.edu.co')

        imagen = self.browser.find_element_by_id('id_imagen')
        imagen.send_keys(
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ05VMx8IQ076_-RLMUnbks_lyGTa-5ZKN3xlsSstInNE5R1RtAFg")

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)

    def test_comentarios(self):
        self.browser.get('http://localhost:8000/')
        comment = self.browser.find_element_by_id('comentario')
        comment.send_keys('Comentarion numero 1')
        correo = self.browser.find_element_by_id('correo')
        correo.send_keys('apbonillab@gmail.com')
        link = self.browser.find_element_by_id('id_comentarios')
        link.click()
        url = self.browser.current_url;
        self.assertIn('http://localhost:8000/detail', url)
        correoFinal = self.browser.find_element_by_id('correo')
        self.assertIn('', correo)


from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^editar/(?P<idTrabajador>\d+)$', views.editar_perfil, name='editar'),
    url(r'^registro', views.register),
    url(r'^login', views.login),
    url(r'^logout$', views.logout),
    url(r'^trabajador/(?P<pk>\d+)$', views.detail),
    url(r'^detail', views.detalle_trabajador),
    url(r'^mostrarTrabajadores/(?P<tipo>\w+)$', views.mostrarTrabajadores),
    url(r'^mostrarTrabajadores', views.mostrarTrabajadores),
    url(r'^tipoServicio/(?P<pk>\d+)$', views.getTiposDeServicio),

]